#!/usr/bin/perl

use strict;
use warnings;
use feature qw(:5.22);

open my $cargo, '<', $ARGV[0] or die "can't open $ARGV[0]: $!\n";
my $data = '';
# This block reads in the entire file.
{
	local $/;
	$data = <$cargo>;
}

my $name = '';
my %cargo = ();
my @lines = split /\r?\n/, $data;

# Parse the file; we just want the name and version.
foreach my $line (@lines) {
	if ($line =~ /^name = "(.*?)"$/) {
		$name = $1;
	} elsif ($line =~ /^version = "(.*?)"$/) {
		$cargo{$name} = $1;
	}
}

open my $file, '>', 'crates.txt' or die "can't open output file: $!\n";
# Print the data we pulled out above to a file.
foreach my $pkg (keys %cargo) {
	say $file $pkg . ' ' . $cargo{$pkg};
}
