# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=e2fsprogs
pkgver=1.46.5
pkgrel=0
pkgdesc="Ext2/3/4 filesystem utilities"
url="http://e2fsprogs.sourceforge.net"
arch="all"
license="GPL-2.0-only AND LGPL-2.0-only AND MIT"
depends=""
depends_dev="util-linux-dev"
makedepends="$depends_dev linux-headers"
subpackages="$pkgname-lang $pkgname-dev $pkgname-doc libcom_err $pkgname-libs"
source="https://www.kernel.org/pub/linux/kernel/people/tytso/$pkgname/v$pkgver/$pkgname-$pkgver.tar.xz
	header-fix.patch
	time64.patch
	"

# secfixes:
#   1.45.3-r1:
#     - CVE-2019-5094

build () {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--mandir=/usr/share/man \
		--enable-elf-shlibs \
		--enable-symlink-install \
		--disable-fsck \
		--disable-uuidd \
		--disable-libuuid \
		--disable-libblkid \
		--disable-tls
	make
}

check() {
	make check
}

package() {
	make -j1 LDCONFIG=: DESTDIR="$pkgdir" install install-libs
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/bin
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/bin/compile_et "$pkgdir"/usr/bin/mk_cmds \
		"$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/share/et "$pkgdir"/usr/share/ss \
		"$subpkgdir"/usr/share
}

libcom_err() {
	pkgdesc="Common error description library"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/libcom_err* "$subpkgdir"/lib/
}

sha512sums="53282e1c524f62a95012b1aceef296d494f5238c82c9b08b29fbe6a803dbf7ccfdcd9124eb2f11fe2ff9de26387c78751a92c3217ca414f6db6242098a72d3fa  e2fsprogs-1.46.5.tar.xz
34ca45c64a132bb4b507cd4ffb763c6d1b7979eccfed20f63417e514871b47639d32f2a3ecff090713c21a0f02ac503d5093960c80401d64081c592d01af279d  header-fix.patch
a3cf5ce222fce3d2655d4cac3cbead1f5eb0f7da5d10f2d9ed3a7f50b5970b74e559af47d50802270c853451e850177772fab3d7c495aa12fbd0aa85d4c34cf2  time64.patch"
