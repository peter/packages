# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=parted
pkgver=3.5
pkgrel=0
pkgdesc="Utility to create, destroy, resize, check and copy partitions"
url="https://www.gnu.org/software/parted/parted.html"
arch="all"
license="GPL-3.0+"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
depends=""
makedepends="ncurses-dev lvm2-dev bash util-linux-dev autoconf automake"
checkdepends="check-dev e2fsprogs python3"
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	fix-includes.patch
	posix-shell.patch
	sysmacros.patch
	tests-call-name-correctly.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-debug \
		--enable-static \
		--enable-shared \
		--enable-device-mapper \
		--without-readline
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="87fc69e947de5f0b670ee5373a7cdf86180cd782f6d7280f970f217f73f55ee1b1b018563f48954f3a54fdde5974b33e07eee68c9ccdf08e621d3dc0e3ce126a  parted-3.5.tar.xz
55ee63c218d1867c0f2c596e7c3eec5c42af160181456cc551fe3d432eabed0ac2dd3a3955ff0c375f76aeec8071e7f55a32834b87a0d39b8ef30361f671bfdd  fix-includes.patch
507d19454aca6631fe387ca53b28f2925c325044efc7b506e81429a5b42914e56c39e1644c570840b4c4c153da742237428cba2a1ff651487f2ef8a0a8c64d1f  posix-shell.patch
5d2e8f22b6cd5bdd3289996848279a945ca09acd2862e82283bb769c2e4d61a24a31e1793d81385e8f3f1f4d48417e2308c5ea39dac47e832666363dde044ba7  sysmacros.patch
8bd86d2b0401566e7757c43d849b7f913cc4ec1bf50d5641dc72d7e278ca38db2ac746cd8dcc756b245021ea1f9738875b6a831f05185b9217d3f1c287944748  tests-call-name-correctly.patch"
