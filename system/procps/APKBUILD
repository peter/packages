# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=procps
pkgver=3.3.17
pkgrel=0
pkgdesc="Utilities for monitoring your system and processes on your system"
url="https://gitlab.com/procps-ng/procps"
arch="all"
license="GPL-2.0+ LGPL-2.0+"
depends=""
checkdepends="dejagnu"
makedepends_build="autoconf automake libtool gettext-tiny"
makedepends_host="ncurses-dev utmps-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang libproc"
source="https://gitlab.com/procps-ng/procps/-/archive/v$pkgver/procps-v$pkgver.tar.bz2
	use-utmpx.patch
	add-langinfo-header.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare
	printf "$pkgver" > .tarball-version
	./autogen.sh
}

build() {
	export LIBS="$LIBS -lutmps -lskarnet"
	export VERSION="$pkgver"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/ \
		--bindir=/bin \
		--sbindir=/sbin \
		--libdir=/lib \
		--mandir=/usr/share/man \
		--sysconfdir=/etc \
		--docdir=/usr/share/doc \
		--datarootdir=/usr/share \
		--disable-static \
		--disable-rpath \
		--with-ncurses \
		ac_cv_func_malloc_0_nonnull=yes \
		ac_cv_func_realloc_0_nonnull=yes
	make
}

check() {
	# can't use `make check`; 'po' dir has no check target
	make check-TESTS
}

package() {
	make DESTDIR="$pkgdir" ldconfig=true install="install -D" \
		install

	# These binaries are identical.
	ln -sf pgrep "$pkgdir"/bin/pkill

	install -d "$pkgdir"/usr/lib
	mv "$pkgdir"/include "$pkgdir"/usr/ \
		&& mv "$pkgdir"/lib/pkgconfig "$pkgdir"/usr/lib/
}

libproc() {
	pkgdesc="Library for monitoring system and processes"
	license="LGPL-2.1+"

	install -d "$subpkgdir"/
	mv "$pkgdir"/lib "$subpkgdir"/
}

sha512sums="7f4ad6a76b042a2478fa866ea63d6826129b08b734c6341155134fcc73c0cf0df429c369dbd5aa65b28ebeeb1ab6ee8df4f2dab2083951bad539bce916e14b64  procps-v3.3.17.tar.bz2
77cfd89cf75e937806dd068a83da3ad548000b941401fce49a482988dc8db82438ade2a939cf6fe795a59059275ffc4a61520b169da60f53ecef408a24042bcf  use-utmpx.patch
f1085cec2fab869b94d2a4b45baf68d6316e8f2ac729cd23341ef27c871df91f83f8953a777a0e858b244d8faf5171f9c640b9e7616c9a5d9c816fa044b92a78  add-langinfo-header.patch"
