# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=psmisc
pkgver=23.4
pkgrel=0
pkgdesc="Miscellaneous utilities that use the proc filesystem"
url="https://gitlab.com/psmisc/psmisc"
arch="all"
options="!check"  # killall(8) is known-broken on musl:
                  # https://gitlab.com/psmisc/psmisc/issues/18
license="GPL-2.0+"
depends=""
checkdepends="dejagnu"
makedepends_build="autoconf>=2.69 automake"
makedepends_host="ncurses-dev"
subpackages="$pkgname-doc"
[ "$CBUILD" != "$CHOST" ] || subpackages="$subpackages $pkgname-lang"
source="$pkgname-$pkgver.tar.bz2::https://gitlab.com/psmisc/psmisc/-/archive/v$pkgver/psmisc-v$pkgver.tar.bz2
	fix-peekfd-on-ppc.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare
	sh autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-harden-flags \
		--enable-ipv6 \
		--disable-selinux \
		ac_cv_func_malloc_0_nonnull=yes \
		ac_cv_func_realloc_0_nonnull=yes
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3632cb12d2de604974229f9e4707cde4db467fafff326e76e510251fe262fa4f1b1edd2d46443733996a17af6a57daf46ae98a6088a829f4ae52222da5e9963a  psmisc-23.4.tar.bz2
a910611896368a088503f50a04a1c2af00d57ee20f3613e81c79cd89574805a505dff43e356ed833a464e3b59d7c1e11fd52cf0bbf32fcfece4dbd2380f23b71  fix-peekfd-on-ppc.patch"
