#!/bin/sh -e

##
# This script downloads the "base" ncurses tarball and all patches (if any)
# up to and including the version specified in '_ver', to a temporary dir.
#
# It prints a first pass at 'sources=' and the corresponding checksums. You
# need to copy/paste these into the APKBUILD yourself. Ensure tabs!!!
#

##
# Only run from this directory.
#
test -f APKBUILD || exit 1;
. ./APKBUILD

##
# Mirror (note: not invisible-island, related).
#
m=https://invisible-mirror.net/archives/ncurses

##
# Parse the HTML from directory index to determine available patches.
#
p=$(curl -s ${m}/${pkgver}/ \
    | grep -oE '[0-9]{8}\.patch' `# 8-digit date` \
    | sort \
    | uniq \
    | awk "\$1 <= ${_ver} + 1" `# exclude newly-available patches` \
    | awk "{print \"${m}/${pkgver}/${pkgname}-${pkgver}-\"\$1\".gz\"}"
);

d=$(mktemp -d);
cd ${d};

##
# Download "base" tarball.
#
s=${m}/${pkgname}-${pkgver}.tar.gz;
curl -sq -O ${s};

##
# Generate 'source=', extracting patches along the way.
#
printf "source=\"%s\n" "${s}";
for k in ${p}; do
    curl -sq -O ${k};
    gzip -d ${k##*/};
    _k=${k##*/};
    printf "\t%s\n" "${_k%.*}";
done
printf "\t\"\n";

##
# Checksums.
#
sha512sum $(find . -type f -print | cut -d/ -f2- | sort);
rm "${s##*/}";

##
# Cleanup.
#
printf "\n";
printf "YOU MUST COPY THE PATCHES FROM '%s' (and delete that dir after)!\n" "${d}";
