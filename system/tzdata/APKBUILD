# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tzdata
pkgver=2022a
pkgrel=0
pkgdesc="Time zone data"
url="https://www.iana.org/time-zones"
arch="all"
options="!check"  # Until we have sgml-common.
license="Public-Domain"
depends=""
checkdepends="sgml-common sp"
makedepends=""
subpackages="$pkgname-doc"
source="https://www.iana.org/time-zones/repository/releases/tzcode$pkgver.tar.gz
	https://www.iana.org/time-zones/repository/releases/tzdata$pkgver.tar.gz
	"

builddir="$srcdir"
_timezones="africa antarctica asia australasia europe northamerica \
	southamerica etcetera backward factory"

build() {
	make cc="${CC:-gcc}" CFLAGS="$CFLAGS -DHAVE_STDINT_H=1"
		TZDIR="/usr/share/zoneinfo"
}

check() {
	make -j1 check
}

package() {
	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo ${_timezones}
	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo/right -L leapseconds ${_timezones}
	#./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo/posix ${_timezones}

	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo -p America/New_York
	install -m444 -t "$pkgdir"/usr/share/zoneinfo iso3166.tab zone1970.tab zone.tab

	mkdir -p "$pkgdir"/usr/sbin
	install -m755 zic zdump "$pkgdir"/usr/sbin

	mkdir -p "$pkgdir"/usr/share/man/man8
	install -m644 zic.8 zdump.8 "$pkgdir"/usr/share/man/man8

	rm -f "$pkgdir"/usr/share/zoneinfo/localtime
}

sha512sums="3f047a6f414ae3df4a3d6bb9b39a1790833d191ae48e6320ab9438cd326dc455475186a02c44e4cde96b48101ab000880919b1e0e8092aed7254443ed2c831ed  tzcode2022a.tar.gz
542e4559beac8fd8c4af7d08d816fd12cfe7ffcb6f20bba4ff1c20eba717749ef96e5cf599b2fe03b5b8469c0467f8cb1c893008160da281055a123dd9e810d9  tzdata2022a.tar.gz"
