commit ff5587d651371ab496f7962e85fe2c337fdb8a59
Author: Brian Behlendorf <behlendorf1@llnl.gov>
Date:   Wed Feb 26 12:42:33 2020 -0800

    Linux 5.6 compat: ktime_get_raw_ts64()
    
    The getrawmonotonic() and getrawmonotonic64() interfaces have been
    fully retired.  Update gethrtime() to use the replacement interface
    ktime_get_raw_ts64() which was introduced in the 4.18 kernel.
    
    Reviewed-by: Tony Hutter <hutter2@llnl.gov>
    Signed-off-by: Brian Behlendorf <behlendorf1@llnl.gov>
    Closes #10052
    Closes #10064

diff --git a/config/kernel-ktime.m4 b/config/kernel-ktime.m4
new file mode 100644
index 000000000..64c3b5f90
--- /dev/null
+++ b/config/kernel-ktime.m4
@@ -0,0 +1,55 @@
+dnl #
+dnl # 4.18: ktime_get_coarse_real_ts64() replaces current_kernel_time64().
+dnl #
+AC_DEFUN([ZFS_AC_KERNEL_SRC_KTIME_GET_COARSE_REAL_TS64], [
+	ZFS_LINUX_TEST_SRC([ktime_get_coarse_real_ts64], [
+		#include <linux/mm.h>
+	], [
+		struct timespec64 ts;
+		ktime_get_coarse_real_ts64(&ts);
+	])
+])
+
+AC_DEFUN([ZFS_AC_KERNEL_KTIME_GET_COARSE_REAL_TS64], [
+	AC_MSG_CHECKING([whether ktime_get_coarse_real_ts64() exists])
+	ZFS_LINUX_TEST_RESULT([ktime_get_coarse_real_ts64], [
+		AC_MSG_RESULT(yes)
+		AC_DEFINE(HAVE_KTIME_GET_COARSE_REAL_TS64, 1,
+		    [ktime_get_coarse_real_ts64() exists])
+	], [
+		AC_MSG_RESULT(no)
+	])
+])
+
+dnl #
+dnl # 4.18: ktime_get_raw_ts64() replaces getrawmonotonic64().
+dnl #
+AC_DEFUN([ZFS_AC_KERNEL_SRC_KTIME_GET_RAW_TS64], [
+	ZFS_LINUX_TEST_SRC([ktime_get_raw_ts64], [
+		#include <linux/mm.h>
+	], [
+		struct timespec64 ts;
+		ktime_get_raw_ts64(&ts);
+	])
+])
+
+AC_DEFUN([ZFS_AC_KERNEL_KTIME_GET_RAW_TS64], [
+	AC_MSG_CHECKING([whether ktime_get_raw_ts64() exists])
+	ZFS_LINUX_TEST_RESULT([ktime_get_raw_ts64], [
+		AC_MSG_RESULT(yes)
+		AC_DEFINE(HAVE_KTIME_GET_RAW_TS64, 1,
+		    [ktime_get_raw_ts64() exists])
+	], [
+		AC_MSG_RESULT(no)
+	])
+])
+
+AC_DEFUN([ZFS_AC_KERNEL_SRC_KTIME], [
+	ZFS_AC_KERNEL_SRC_KTIME_GET_COARSE_REAL_TS64
+	ZFS_AC_KERNEL_SRC_KTIME_GET_RAW_TS64
+])
+
+AC_DEFUN([ZFS_AC_KERNEL_KTIME], [
+	ZFS_AC_KERNEL_KTIME_GET_COARSE_REAL_TS64
+	ZFS_AC_KERNEL_KTIME_GET_RAW_TS64
+])
diff --git a/config/kernel-ktime_get_coarse_real_ts64.m4 b/config/kernel-ktime_get_coarse_real_ts64.m4
deleted file mode 100644
index 28492bf04..000000000
--- a/config/kernel-ktime_get_coarse_real_ts64.m4
+++ /dev/null
@@ -1,23 +0,0 @@
-dnl #
-dnl # 4.18: ktime_get_coarse_real_ts64() added.  Use it in place of
-dnl # current_kernel_time64().
-dnl #
-AC_DEFUN([ZFS_AC_KERNEL_SRC_KTIME_GET_COARSE_REAL_TS64], [
-	ZFS_LINUX_TEST_SRC([ktime_get_coarse_real_ts64], [
-		#include <linux/mm.h>
-	], [
-		struct timespec64 ts;
-		ktime_get_coarse_real_ts64(&ts);
-	])
-])
-
-AC_DEFUN([ZFS_AC_KERNEL_KTIME_GET_COARSE_REAL_TS64], [
-	AC_MSG_CHECKING([whether ktime_get_coarse_real_ts64() exists])
-	ZFS_LINUX_TEST_RESULT([ktime_get_coarse_real_ts64], [
-		AC_MSG_RESULT(yes)
-		AC_DEFINE(HAVE_KTIME_GET_COARSE_REAL_TS64, 1,
-		    [ktime_get_coarse_real_ts64() exists])
-	], [
-		AC_MSG_RESULT(no)
-	])
-])
diff --git a/config/kernel.m4 b/config/kernel.m4
index 6bb37937c..a0fa2514e 100644
--- a/config/kernel.m4
+++ b/config/kernel.m4
@@ -114,7 +114,7 @@ AC_DEFUN([ZFS_AC_KERNEL_TEST_SRC], [
 	ZFS_AC_KERNEL_SRC_CURRENT_TIME
 	ZFS_AC_KERNEL_SRC_USERNS_CAPABILITIES
 	ZFS_AC_KERNEL_SRC_IN_COMPAT_SYSCALL
-	ZFS_AC_KERNEL_SRC_KTIME_GET_COARSE_REAL_TS64
+	ZFS_AC_KERNEL_SRC_KTIME
 	ZFS_AC_KERNEL_SRC_TOTALRAM_PAGES_FUNC
 	ZFS_AC_KERNEL_SRC_TOTALHIGH_PAGES
 	ZFS_AC_KERNEL_SRC_KSTRTOUL
@@ -208,7 +208,7 @@ AC_DEFUN([ZFS_AC_KERNEL_TEST_RESULT], [
 	ZFS_AC_KERNEL_CURRENT_TIME
 	ZFS_AC_KERNEL_USERNS_CAPABILITIES
 	ZFS_AC_KERNEL_IN_COMPAT_SYSCALL
-	ZFS_AC_KERNEL_KTIME_GET_COARSE_REAL_TS64
+	ZFS_AC_KERNEL_KTIME
 	ZFS_AC_KERNEL_TOTALRAM_PAGES_FUNC
 	ZFS_AC_KERNEL_TOTALHIGH_PAGES
 	ZFS_AC_KERNEL_KSTRTOUL
diff --git a/include/spl/sys/time.h b/include/spl/sys/time.h
index 312415b7b..56fd725c0 100644
--- a/include/spl/sys/time.h
+++ b/include/spl/sys/time.h
@@ -105,8 +105,13 @@ gethrestime_sec(void)
 static inline hrtime_t
 gethrtime(void)
 {
+#if defined(HAVE_KTIME_GET_RAW_TS64)
+	struct timespec64 ts;
+	ktime_get_raw_ts64(&ts);
+#else
 	struct timespec ts;
 	getrawmonotonic(&ts);
+#endif
 	return (((hrtime_t)ts.tv_sec * NSEC_PER_SEC) + ts.tv_nsec);
 }
 
