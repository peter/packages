# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmymoney
pkgver=5.0.1
pkgrel=0
pkgdesc="Personal finance manager from KDE"
url="https://kmymoney.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	gpgme-dev

	akonadi-dev kactivities-dev karchive-dev kcmutils-dev kcompletion-dev
	kconfig-dev kconfigwidgets-dev kcontacts-dev kcoreaddons-dev
	kdoctools-dev kholidays-dev ki18n-dev kiconthemes-dev kio-dev
	kitemmodels-dev kitemviews-dev knotifications-dev kservice-dev
	ktextwidgets-dev kwallet-dev kxmlgui-dev

	alkimia-dev kdewebkit-dev kdiagram-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/kmymoney/$pkgver/src/kmymoney-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="74ace8d98f144c503d572c68cf50c27664a49bc5f4428788b69a1da52b8f72e97e1bb70914dbf041c43884c3b6340e28390618dec179ebe3d1b3ea63bede93e7  kmymoney-5.0.1.tar.xz"
