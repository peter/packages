# Contributor: Ivan Tham <pickfire@riseup.net>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=tlp
_pkgname=TLP
pkgver=1.3.1
pkgrel=0
pkgdesc="Linux Advanced Power Management"
url="https://linrunner.de/en/tlp/tlp.html"
arch="noarch"
options="!check"  # No test suite.
license="GPL-2.0+ AND GPL-3.0+"
depends="/bin/sh perl"
makedepends=""
subpackages="$pkgname-doc $pkgname-rdw $pkgname-bash-completion:bashcomp
	$pkgname-openrc"
install="$pkgname.post-upgrade"
source="$pkgname-$pkgver.tar.gz::https://github.com/linrunner/$_pkgname/archive/$pkgver.tar.gz
	$pkgname.initd"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	make
}

package() {
	make DESTDIR="$pkgdir" TLP_WITH_SYSTEMD=0 install-tlp install-man
	install -Dm755 "$srcdir"/tlp.initd "$pkgdir"/etc/init.d/"$pkgname"
	# We don't ship systemd
	find "$pkgdir"/usr/share/man/man8 -name '*.service*' -delete
	rm -r "$pkgdir"/lib/elogind
}

rdw() {
	pkgdesc="Linux Advanced Power Management - Radio Device Wizard"
	depends="tlp"

	make DESTDIR="$subpkgdir" TLP_WITH_SYSTEMD=0 -C "$builddir" install-rdw
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/bash-completion/completions
	mv "$pkgdir"/usr/share/bash-completion/completions/* \
		"$subpkgdir"/usr/share/bash-completion/completions
	mv "$pkgbasedir/$pkgname-rdw"/usr/share/bash-completion/completions/* \
		"$subpkgdir"/usr/share/bash-completion/completions
}

sha512sums="7ebb14ea797ad8aae613c537de51107a3c7430b5c6ee1407e722e2069ed5376f88ab3dac613651fafabd92d219b980452f236bef33e8a655b1abbfed5eded1b1  tlp-1.3.1.tar.gz
e6de216b2540413812711b3304cdc29c8729d527080cfd747ba382db50166dd21c6c27ff467f9f2a967e92007c7a311b00e88262952c34a22f417578c66cf4e7  tlp.initd"
