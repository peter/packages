# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer:
pkgname=opencv
pkgver=4.6.0
pkgrel=0
pkgdesc="Computer vision and machine learning software library"
url="https://opencv.org"
arch="all"
options="!check"  # an hour on the talos is madness
license="BSD-3-Clause"
depends=""
makedepends="cmake doxygen ffmpeg-dev gst-plugins-base-dev gtk+2.0-dev
	gtk+3.0-dev jasper-dev libdc1394-dev libgomp libgphoto2-dev
	libjpeg-turbo-dev libpng-dev libwebp-dev tiff-dev v4l-utils-dev
	mawk protobuf-dev"
subpackages="$pkgname-dev $pkgname-libs"
source="opencv-$pkgver.tar.gz::https://github.com/opencv/opencv/archive/$pkgver.tar.gz
	ade-0.1.1f.zip::https://github.com/opencv/ade/archive/v0.1.1f.zip
	cmake-license.patch
	"

# secfixes:
#   4.1.1-r1:
#     - CVE-2019-16249

prepare() {
	default_prepare
	# purge 3rd party except carotene
	for i in 3rdparty/*; do
		case $i in
		*/carotene*) continue;;
		*/ittnotify) continue;;  # Else FTBFS on x86_64
		*/quirc)     continue;;
		esac
		rm -rf "$i"
	done
	mkdir -p build
	mkdir -p .cache/ade
	cp "$srcdir"/ade-0.1.1f.zip \
		"$builddir"/.cache/ade/$(md5sum "$srcdir"/ade-0.1.1f.zip | awk '{print $1;}')-0.1.1f.zip
}

build() {
	_sse=""
	if [ "$CARCH" != "x86_64" ]; then
		_sse="-DENABLE_SSE=OFF -DENABLE_SSE2=OFF"
	fi
	if [ "$CARCH" = "ppc" ]; then
		export LDFLAGS="$LDFLAGS -latomic"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_PROTOBUF=OFF \
		-DPROTOBUF_UPDATE_FILES=ON \
		-DENABLE_PRECOMPILED_HEADERS=OFF \
		-DWITH_OPENMP=ON \
		-DWITH_OPENCL=ON \
		-DWITH_OPENEXR=OFF \
		-DWITH_OPENJPEG=OFF \
		-DWITH_IPP=OFF \
		$_sse \
		-Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="93d7807794682990b6a1d1de1851986ae3c5d1afe6605f3f8cace03ba5e3390bee2568bc0f335af34d3fc974df64cbce0ce685261ec2abd693d259b97b15bc46  opencv-4.6.0.tar.gz
f2994d5e92a2ae05cee6e153943afe151ce734ced6e06dcdb02dee9fed9336a7f1ea69661d9e033f1412fbb5e2a44a6e641662c85be5ba0604d0446abeabe836  ade-0.1.1f.zip
ffa6930086051c545a44d28b8e428de7faaeecf961cdee6eef007b2b01db7e5897c6f184b1059df9763c1bcd90f88b9ead710dc13b51a608f21d683f55f39bd6  cmake-license.patch"
