# Maintainer: Zach van Rijn <me@zv.io>
pkgname=guile
pkgver=2.2.7
pkgrel=1
pkgdesc="Portable, embeddable Scheme implementation written in C"
url="https://www.gnu.org/software/guile/"
arch="all"
options="!check !dbg !strip"  # Requires actual LC_COLLATE and LC_MONETARY support.
license="LGPL-3.0+ AND GPL-3.0+"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
depends=""
depends_dev="guile gc-dev"
makedepends="gc-dev gmp-dev libffi-dev libtool libunistring-dev ncurses-dev
	texinfo"
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.gz
	0002-Mark-mutex-with-owner-not-retained-threads-test-as-u.patch
	"

prepare() {
	default_prepare

	case "$CARCH" in
		ppc)	rm -fvr prebuilt/32-bit-big-endian; ;; #533
	esac
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-error-on-warning \
		--disable-static
	make
}

package() {
	make DESTDIR="$pkgdir" install
	rm "$pkgdir"/usr/lib/charset.alias
}

sha512sums="ad11885ffeb7655ef6c8543e67233992dc37bdcf91ed82188e6a144169c6b7d4e31cf7a6d01509c573d00904cb002719b851f71cdf1359a86de401daf613d773  guile-2.2.7.tar.gz
b1c309cc07830ff1741ef88857f8099187b449580e8d57862886abc367ef1accc5a35636d81eee09247f13d3a751cdc8909fdea05368d3d509bd2039ce06d078  0002-Mark-mutex-with-owner-not-retained-threads-test-as-u.patch"
