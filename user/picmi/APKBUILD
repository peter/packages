# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=picmi
pkgver=22.04.2
pkgrel=0
pkgdesc="Single-player logic/pattern game"
url="https://kde.org/applications/games/org.kde.picmi"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev qt5-qtsvg-dev cmake extra-cmake-modules
	qt5-qtdeclarative-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev
	kdeclarative-dev kdoctools-dev ki18n-dev kio-dev knewstuff-dev
	kxmlgui-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/picmi-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild .
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="10c3ffe181a2aa16aa2f6d7cf2bc4c739bfd586265312484247e658a3d02042af81dce67bf9da9b776bf2785f97d213dd2ee3f0175e2fbaf07a85b1b980045ae  picmi-22.04.2.tar.xz"
