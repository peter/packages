# Contributor: Russ Webber <russ@rw.id.au>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=assimp
pkgver=5.1.5
pkgrel=0
pkgdesc="Open Asset Import Library imports and exports 3D model formats"
url="http://www.assimp.org/"
arch="all" # FIXME: might fail to build on big-endian?
license="BSD-3-Clause"
depends=""
makedepends="cmake minizip-dev zlib-dev"
subpackages="$pkgname-dev"
source="https://github.com/assimp/assimp/archive/v$pkgver/assimp-v$pkgver.tar.gz
	01-rm-revision-test.patch
	05-remove-failing-x86-test.patch
	restore-pbrmaterial-glossy-macro.patch
	"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DASSIMP_BUILD_TESTS=True
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
	rm -vf "$pkgdir"/usr/lib/libIrrXML.a
}
sha512sums="07e39de36340e5651c00f4b4363abd27dae5c3b5cdc4aba7a294e360a5724a3574975fb856e60c185f06d8510648ac3c0ed8d03ed03727ec3725f796c17aeafa  assimp-v5.1.5.tar.gz
535a80c5899a2994735e7b17b4e3fdb3ff2b53e64c09ec8fcab1dbcea2ad696fed50aeb1667a3c4e00a8a3ddf411d33ec1d36fcb256e26f02cf951f0e2c83a73  01-rm-revision-test.patch
bfb321b8493b2ef8c35ab0627173d0f1754519e9bae6f6c59cc164aaf91a07d8ae382fbc92550cfab6f52502565390ba22b7478efa5bba008eec0aa075c5e7d1  05-remove-failing-x86-test.patch
d582ccbf645e0ac133cdc27a47da7b281202b1615c014e32c823dace03f2a58658434e5d93409151f19f45ea304e28b1a22477cdc566375c82b32cf046a6bb9d  restore-pbrmaterial-glossy-macro.patch"
