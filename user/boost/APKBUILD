# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=boost
pkgver=1.79.0
_pkgver=$(printf '%s' "$pkgver" | tr . _)
pkgrel=1
pkgdesc="Free peer-reviewed portable C++ source libraries"
url="https://www.boost.org/"
arch="all"
license="BSL-1.0"
depends=""
depends_dev="linux-headers"
makedepends="$depends_dev byacc bzip2-dev flex python3-dev xz-dev zlib-dev
	zstd-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://boostorg.jfrog.io/artifactory/main/release/${pkgver}/source/${pkgname}_${_pkgver}.tar.bz2
	python-311-fix-enum.patch
	python-311-fix-open.patch
	"

builddir="$srcdir/${pkgname}_${_pkgver}"

_libs="
	atomic
	chrono
	container
	context
	contract
	coroutine
	date_time
	fiber
	filesystem
	graph
	iostreams
	json
	locale
	log_setup
	log
	math
	nowide
	prg_exec_monitor
	program_options
	python3
	random
	regex
	serialization
	stacktrace_basic
	stacktrace_noop
	system
	thread
	timer
	type_erasure
	unit_test_framework
	wave
	wserialization
	"
for _lib in $_libs; do
	subpackages="$subpackages $pkgname-${_lib}:_boostlib"
done

prepare() {
	default_prepare

	PY3_VERSION="$(_pyversion python3)"
	abiflags="$(python3-config --abiflags)"

	# create user-config.jam
	cat > user-config.jam <<-__EOF__

	using gcc : : $CC : <cxxflags>"$CXXFLAGS" <linkflags>"$LDFLAGS" ;
	using python : ${PY3_VERSION} : /usr/bin/python3 : /usr/include/python${PY3_VERSION}${abiflags} : "$(python3-config --libs)" : : $abiflags ;

	__EOF__
}

case "$CARCH" in
	s390x)
		_options_s390="--without-coroutine --without-coroutine2" ;;
esac
_enginedir=tools/build/src/engine
_bjam="${builddir}/$_enginedir/bjam"

_options="--user-config=\"$builddir/user-config.jam\"
	--prefix=\"$pkgdir/usr\"
	release
	runtime-link=shared
	link=shared,static
	cflags=-fno-strict-aliasing
	-sPYTHON_ROOT=/usr
	-sTOOLS=gcc
	--layout=system
	-q
	--without-mpi
	-j${JOBS:-2}
	${_options_s390}
	"

build() {
	export BOOST_ROOT="$builddir"

	msg "Building bjam"
	cd "$builddir"/$_enginedir
	CC= ./build.sh gcc

	msg "Building bcp"
	cd "$builddir"/tools/bcp
	"$_bjam" -j${JOBS:-2}

	msg "Building boost"
	cd "$builddir"
	"$_bjam" $_options
}

check() {
	cd "$builddir"/tools/build/test

	PATH="${_bjam%/*}:$PATH" python3 test_all.py --default-bjam
}

package() {
	export BOOST_ROOT="$builddir"

	install -Dm755 $_bjam \
		"$pkgdir"/usr/bin/bjam

	install -Dm755 dist/bin/bcp "$pkgdir"/usr/bin/bcp

	install -Dm644 LICENSE_1_0.txt \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE_1_0.txt

	"$pkgdir"/usr/bin/bjam $_options \
		--includedir="$pkgdir"/usr/include \
		--libdir="$pkgdir"/usr/lib \
		install
}

_boostlib() {
	libname="${subpkgname#$pkgname-}"
	pkgdesc="Boost library - $libname"
	case "$subpkgname" in
	boost-python3) depends="$depends python3"
	esac

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libboost_$libname* "$subpkgdir"/usr/lib/
}

_pyversion() {
	$1 -c 'import sys; print("%i.%i" % (sys.version_info.major, sys.version_info.minor))'
}

sha512sums="70909e0561c213d10a1fdd692f9ae7b293d3cdc63e925bdc207da9e9bba6e86474341100e7ee5de6d94f9561196b1a3a1597055a7b7382babf8931131170a312  boost_1_79_0.tar.bz2
04b9bcbd37cf660450aa4ff27b32db514d669caa378a25158d6d1e71ef0b42ea097e9dfda01774fbe80487db0f80d809d10b42e5ebc464f9faff02379bead98d  python-311-fix-enum.patch
4b517dc36719e4e4666d32822a65e650496f9ec534232ae96500538bf8182ce72e08bda3786a598330babda1bc169afa9b38fbc9d5a63d2dc633c03ba428a060  python-311-fix-open.patch"
