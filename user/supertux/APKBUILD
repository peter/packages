# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=supertux
pkgver=0.6.3
pkgrel=0
pkgdesc="Platform game featuring Tux the Penugin"
url="https://www.supertux.org/"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="boost-dev cmake curl-dev glew-dev libogg-dev libvorbis-dev zlib-dev
	mesa-dev openal-soft-dev physfs-dev sdl2-dev sdl2_image-dev doxygen
	freetype-dev glm libpng-dev"
subpackages="$pkgname-doc"
source="https://github.com/SuperTux/supertux/releases/download/v$pkgver/SuperTux-v$pkgver-Source.tar.gz
	find-sdl2.patch
	cmake32.patch
	"
builddir="$srcdir"/SuperTux-v$pkgver-Source

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c6540bab1b3befbd975756031c4587e5569d9613d9539dc829c728b574d1a4da92816d6a7e68947b32963cc13d9b8b52312701c199138640e9f89e5885433798  SuperTux-v0.6.3-Source.tar.gz
9e229d837d8006d618320ef3f7465ef88f8903853ed1ef3e291373d25f8542010f7dad6c1c5859e857db48e1447337ddbed0619228f5eac6aba916b69567fd94  find-sdl2.patch
c0e6cdd52e07e04ef67edc8b7f3d3c37dd2ffd35381b704b43512a01bbd0a5a35fbeae7ec225be8e4643dbcaac3eae427d7ea5217dc1e676f2991ee46f5bf513  cmake32.patch"
