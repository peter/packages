# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kreversi
pkgver=22.04.2
pkgrel=0
pkgdesc="Reversi game"
url="https://games.kde.org/game.php?game=kreversi"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev kdeclarative-dev ki18n-dev kiconthemes-dev kio-dev
	kxmlgui-dev kwidgetsaddons-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kreversi-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e03af9b941afe05fd4768b45ce4a19f0f98e7770926e209f80830c99e7c341f42d4df4f74f9884cbee261769bbed47ae707e1b2b4d4db163d2c533c2e57037e1  kreversi-22.04.2.tar.xz"
