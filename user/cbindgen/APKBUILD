# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Gentoo Rust Maintainers <rust@gentoo.org>
# Contributor: Samuel Holland <samuel@sholland.org>
# Contributor: Molly Miller <adelie@m-squa.red>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=cbindgen
pkgver=0.24.3
pkgrel=0
pkgdesc="Tool to generate C bindings from Rust code"
url="https://github.com/eqrion/cbindgen"
arch="all"
license="MPL-2.0"
depends=""
makedepends="cargo"
checkdepends="py3-cython"
source=""

# dependencies taken from Cargo.lock
cargo_deps="$pkgname-$pkgver
hashbrown-0.11.2
strsim-0.10.0
serial_test-0.5.1
syn-1.0.89
hermit-abi-0.1.19
autocfg-1.1.0
libc-0.2.121
ryu-1.0.9
fastrand-1.7.0
winapi-x86_64-pc-windows-gnu-0.4.0
bitflags-1.3.2
parking_lot_core-0.8.5
parking_lot-0.11.2
toml-0.5.8
proc-macro2-1.0.36
log-0.4.16
os_str_bytes-6.0.0
cfg-if-1.0.0
memchr-2.4.1
serial_test_derive-0.5.1
heck-0.4.0
winapi-util-0.1.5
scopeguard-1.1.0
quote-1.0.17
indexmap-1.8.0
instant-0.1.12
winapi-i686-pc-windows-gnu-0.4.0
lazy_static-1.4.0
serde_json-1.0.79
itoa-1.0.1
remove_dir_all-0.5.3
textwrap-0.15.0
winapi-0.3.9
clap-3.1.6
unicode-xid-0.2.2
atty-0.2.14
lock_api-0.4.6
redox_syscall-0.2.12
smallvec-1.8.0
serde-1.0.136
serde_derive-1.0.136
termcolor-1.1.3
tempfile-3.3.0
"

source="$source $(echo $cargo_deps | sed -E 's#([[:graph:]]+)-([0-9.]+(-(alpha|beta|rc)[0-9.]+)?)#&.tar.gz::https://crates.io/api/v1/crates/\1/\2/download#g')"

prepare() {
	export CARGO_HOME="$srcdir/cargo-home"
	export CARGO_VENDOR="$CARGO_HOME/adelie"

	(builddir=$srcdir; default_prepare)

	mkdir -p "$CARGO_VENDOR"
	cat <<- EOF > "$CARGO_HOME/config"
		[source.adelie]
		directory = "${CARGO_VENDOR}"

		[source.crates-io]
		replace-with = "adelie"
		local-registry = "/nonexistant"
	EOF

	for _dep in $cargo_deps; do
		ln -s "$srcdir/$_dep" "$CARGO_VENDOR/$_dep"
		_sum=$(sha256sum "$srcdir/$_dep.tar.gz" | cut -d' ' -f1)
		cat <<- EOF > "$CARGO_VENDOR/$_dep/.cargo-checksum.json"
		{
			"package":"$_sum",
			"files":{}
		}
		EOF
	done
}

build() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo build -j $JOBS --release
}

check() {
	export CARGO_HOME="$srcdir/cargo-home"

#	# Failing tests
#	rm -rf tests/rust/expand*
	cargo test -j $JOBS --release
}

package() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo install --path . --root="$pkgdir"/usr
	rm "$pkgdir"/usr/.crates.toml
}


sha512sums="3a39be67a87aa7a4dd9baaf6b803215f4587bd7925c4315c5ec93954e021471919fa977ad2084f099c606daa392350de3557bba56cef77806def99c40318ef05  cbindgen-0.24.3.tar.gz
c21ca68fd49bbb741901f59fed04cc124b8da99e2a4dfc26e2e5e1140637872b344612a01691bd30cc771575c571be15f756c84dde225441699cd2322af2ad6c  hashbrown-0.11.2.tar.gz
78b318532addfcf5c1ccc1e14539e258aab9d3cd893cc45d82342549bde838c177d90f13c560671f8f32929af47d0b467db35e6876bd7697d8b3f9e055aeeac1  strsim-0.10.0.tar.gz
e1a0f7a24981698eaa6bcce8f951863f76e8a2750aff3191104d092a06021c39d4eb2e9b74e6690b0dba0d674a216ea170efe0a5367d22bdef72c2006f644a4e  serial_test-0.5.1.tar.gz
6b2a9411c015b2b0ed5c5e18cae67f357d1a735e184416c6f8533be191ba65c0963dc9341ba3829195c6616ce71d4a79cbc7768fdbaa98b798b8f67746c6ab4a  syn-1.0.89.tar.gz
1c877fcd562b15d2de9c151fd6c5f3ea4bf48abcb799e6139a180ffad5d64b632f0000d5707bbd92ff23a0e5f349157b9e0f5be8b50f03680b0fa47315dbb78a  hermit-abi-0.1.19.tar.gz
df972c09abbdc0b6cb6bb55b1e29c7fed706ece38a62613d9e275bac46a19574a7f96f0152cccb0239efea04ee90083a146b58b15307696c4c81878cd12de28f  autocfg-1.1.0.tar.gz
aa1ee710b2a4008ead7118e85d7f2d29fab8aa0e1111436db8039a84737727b8d1a8a1cb72acd38abd2656d22a5025046d7be7d8154537f8a503017e0548e953  libc-0.2.121.tar.gz
4e7c2c7ec73da1ddb32e18d36b8159cb4047b9f4feeb975361e7ba68135e671e11419bb7786df789f5d08e5895231d2c98e36035b515f2c64ac40474d08905cb  ryu-1.0.9.tar.gz
6a1a8cd4f6f9bfff07a4ca18ef84839e4427ca9bf9b6733bb15b1b70cf2439820d6a770ae9f3e5e10166a6144449e37e6f3f6ed9acb761688207fd7c53d2c673  fastrand-1.7.0.tar.gz
4a654af6a5d649dc87e00497245096b35a2894ae66f155cb62389902c3b93ddcc5cf7d0d8b9dd97b291d2d80bc686af2298e80abef6ac69883f4a54e79712513  winapi-x86_64-pc-windows-gnu-0.4.0.tar.gz
3c698f757b5cc62f815f9a1cce365c3d2dd88e4db71f331dff8bba86c2865f755b81cb4c9bfc59bd86b7643b0943f9e09a7c4f8ad75eb2ab0e714803d0129f62  bitflags-1.3.2.tar.gz
c4315df551748d1ae77655e4d9f8c90f911498856e5358009e9e02e410bb8085f006f369188b0753a298371ebd74a5c383d848b65e31b55f3462381308c83a00  parking_lot_core-0.8.5.tar.gz
526b176363dffa59501c18324bb723a3846ef5b0ff9bf1d890e40ad10e7023284f7c8012eda87520eaa94515ee828d9ef52692a9ed590a55e176383d6d472f9e  parking_lot-0.11.2.tar.gz
26b7901ee4b7cbb4cf8ea57d365a99ed05e0a76e73452e47d4bcb3b4eeb7bbd393c13eea9ea33dc13622942efcda39acdba9425b74b40c920c9f19673a1f2082  toml-0.5.8.tar.gz
f31b0f2356af2746321c4f808ac9af87d21a5657c103ed6bc1383855e40caf49246cc8ec1edff58eacf193424abfc2163148b7298e527714e65e602f14b2e50a  proc-macro2-1.0.36.tar.gz
b12dfcd8bec2f44864b8174776d3151ddf1fa1d82bc8a54fb155d5af3b2af959aab4899d72835a3c25cf58d6c41cd7f1b16c2accbdc20a0eba7e8be3d1883ee1  log-0.4.16.tar.gz
d2d3741a1190092cf251e035d2c55a7d022d99512160ed4ddccccb44ca85f664f94f8a937a76eff88581b593e2ebaddab6e753009f3046a8a2ebf451418e41d8  os_str_bytes-6.0.0.tar.gz
0fb16a8882fd30e86b62c5143b1cb18ab564e84e75bd1f28fd12f24ffdc4a42e0d2e012a99abb606c12efe3c11061ff5bf8e24ab053e550ae083f7d90f6576ff  cfg-if-1.0.0.tar.gz
d8912e3902a2126f86159bdc998532a2890b882cbb7d59b5a470fffcad4c32281e045f2fff48a235aa4189f1928866bf3d33b699d50866ad6b6c272bba7adb11  memchr-2.4.1.tar.gz
e3f4b3c2eed1b284dbff7447c2f912343f9b95cbd88f3387c0136ca42698b38a607c752277ee4590ded9f73f475325d2652ba67ba029ddd54711d9070ac5f43e  serial_test_derive-0.5.1.tar.gz
33bdbf4ff9ecc4f4d74cf06590e056f4d96bf0d990d5381b9da5b65682b2495ed74e27b45419c2afa986c1f6200909d5175b137ae73ced5cc8ac869e4e1bce8f  heck-0.4.0.tar.gz
7baeb661f397c4693dfa001fdc774b323c51a7c55caad40f2de5112a1cefd1d6151e3df41fa4ee193460a5905917c83d2b1de5fa10b4bd014ad96690af95c0fd  winapi-util-0.1.5.tar.gz
368fa5726df8f42b599993681579a9ffd0196480ee3cd0f9f671e8493f3bedd1e1779bdf2beb329e77e0005fa09b816e3385f309490c0f2781568db275d4d17d  scopeguard-1.1.0.tar.gz
d34fa0c864e3b9007449269ad5ea8736bbaa359e61eeb454e9c1cc30f9739a96778a04421e2a014879aeee417d8b51e20c6c41ebdab6878567af027096ff9ebd  quote-1.0.17.tar.gz
6af44320a80c8256291cc6e3c90311ce67b7f69ce039d640bb3abbcb6057f16eb443a407060ba647d7b16f44214acf59b566772a4802ba5000f036f69ca82a43  indexmap-1.8.0.tar.gz
fae494c00111c51c840f9dd6a10febe403e27ebb933dd16633a213e9c20f2bc11adeb431c71f8a6713bf88f270a010941e15d83df294e658791934f83a5d2407  instant-0.1.12.tar.gz
a672ccefd0730a8166fef1d4e39f9034d9ae426a3f5e28d1f4169fa5c5790767693f281d890e7804773b34acdb0ae1febac33cde8c50c0044a5a6152c7209ec2  winapi-i686-pc-windows-gnu-0.4.0.tar.gz
e124c0521ec7c950f3c4a066821918da7a9c6e711115d98009ae7c351928fdddead852e7596fea5937a9c30e4e4ce8eee7099b20248b5d6e3b2494b6a6d88cb8  lazy_static-1.4.0.tar.gz
83987c1a7eddc6adf8ae359c30e2db1fea98edabd6dc49bde840c37a56258fa845612e8ae270a7dca4cfbe9a3532dc55028091e14cdf1dd3b1ceea4bb86a2dd7  serde_json-1.0.79.tar.gz
8e7bc1e9bf4fc06871b9fe20caad4e0af965477d724f4c8d0e2a3a4d87aedf99f92e4e583a6440ce574d0fb43fc9d6a2e80add52a2f64210c6aa3b402e424295  itoa-1.0.1.tar.gz
50417d6d8a33912193a1ed37eb72b47431b12ae65d2780cdb7080c3d141e63819da13751c3fb737685cea322f70b36d413389c3dc01aa12b4dce615aefed0e2c  remove_dir_all-0.5.3.tar.gz
f44271c542c22f17a4e3a459255f95e6c02d999f7d6bc8414d3973fd4ac9353aa4ef436932a45340738126905463d776902715feaa9329371f8a14f14b5a7bfd  textwrap-0.15.0.tar.gz
ff8b7b78065f3d8999ec03c725a0460ebc059771bf071c7a3df3f0ecd733edf3b0a2450024d4e24e1aedddaecd9038ce1376c0d8bbf45132068cf45cf4a53a97  winapi-0.3.9.tar.gz
397c277eeb9acafc1098f6b6aa7e428cf7c7016356a99139dbff66dd6c6e12434b14a457679fd99ea50c1fb0b54b9b96bad65f1198025986835810484e542824  clap-3.1.6.tar.gz
92ffd0dd34e3ca235ecf110b38c447d3ec1faa23d76c112457f28d432f92fa6b5f428bc5e1bfd278f361f55426dd96e19ecb0d3eff6cf250892f069c52bd89a8  unicode-xid-0.2.2.tar.gz
d7b6c4b9a0f898d91ddbc41a5ee45bbf45d1d269508c8cc87ee3e3990500e41e0ec387afb1f3bc7db55bedac396dd86c6509f4bf9e5148d809c3802edcc5e1d9  atty-0.2.14.tar.gz
ba14d341564eba91bb5ec58bb2e0d6c87e70b3694c4609396f0c0f4afc0c6c105a88cb99c303b1e9761b0fc6cae56f79ba6a7663b063454ab1ede4d49274906d  lock_api-0.4.6.tar.gz
c89b2aa6ab9b34ba10983c9b6c90108e0b7e380357c9b5f50a72ea6ab54311437c88859d2345b28747d2539fdac29a597b72e7e47b4ded9b6dfbc54ea9792048  redox_syscall-0.2.12.tar.gz
17687cfa6aaf95a1df063adc3a412a7c41918a0d003eaac90f7d9e859fb8fa1d652eedee17a4cb3aaae9b33a2043f89e796519e3a7a3992b292f04049bf80b0c  smallvec-1.8.0.tar.gz
d043ccfd6a1dc9a114a1a95530a63ed9342688d31d3aadeec600a9a9d47aad328be35b6f80080400ea4bb3f820ddd18cc5ce1e6ea1db28e02752962061e71019  serde-1.0.136.tar.gz
92c99455a5a9ac0fe45eb6ff1ad3ea55db860031fb912ace3755d1d2aad0dd880b2d83add4d41872823bd60557ebe1cb36e898bf0ac975b1093caa9819f7c289  serde_derive-1.0.136.tar.gz
5838fcbfd70f300cb4b62aab50565db52074c56b152ccc8ac1173e4676c0d5a636271bf5a645a77da6e1d4edbf0091af2cd4dd6d73b85c3d198c760898c06f3a  termcolor-1.1.3.tar.gz
ba6faafb2dd56d694efe424752099a2efb50316afc0a4db9fdb7620ae3f1a31dfbb2a7b41724878cb977fa11f7568a406bd3b6a4f7cfc0b88b86b2cc616b953e  tempfile-3.3.0.tar.gz"
