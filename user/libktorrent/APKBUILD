# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libktorrent
pkgver=22.04.2
pkgrel=0
pkgdesc="Torrent handling library for KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev kcrash-dev kio-dev solid-dev gmp-dev
	boost-dev qca-dev libgcrypt-dev"
makedepends="cmake extra-cmake-modules $depends_dev doxygen ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/libktorrent-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b5ef82b2d2c9235e0760fff0c4b30339abb076f47c75bee85379d247c4b0d0fffd7d194b90ba4594d4ac6461d8045570f0bcf56dd484527338afd94a6ea2eeda  libktorrent-22.04.2.tar.xz"
