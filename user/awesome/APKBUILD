# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=awesome
pkgver=4.3
pkgrel=0
pkgdesc="lua-configurable window manager framework"
url="http://awesome.naquadah.org/"
arch="all"
options="!check"  # Tests require X and D-Bus
license="GPL-2.0+"
depends="imagemagick lua-lgi cairo-gobject pango"
checkdepends="xorg-server-xephyr"
makedepends="lua5.3 lua5.3-dev libxcb-dev pango-dev cairo-dev cmake gperf glib-dev
	imlib2-dev libxdg-basedir-dev libev-dev startup-notification-dev
	xcb-util-keysyms-dev xcb-util-image-dev xcb-util-dev xcb-util-wm-dev
	dbus-dev gdk-pixbuf-dev xcb-util-cursor-dev libxkbcommon-dev
	xcb-util-xrm-dev libexecinfo-dev xmlto"
subpackages="$pkgname-doc"
source="https://github.com/awesomeWM/$pkgname-releases/raw/master/$pkgname-$pkgver.tar.xz"

build() {
	# Awesome does not support in-tree builds
	cmake -DCMAKE_INSTALL_PREFIX=/usr \
		-DSYSCONFDIR=/etc \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
                -DLUA_INCLUDE_DIR=/usr/include/lua5.3 \
                -DLUA_LIBRARIES=/usr/lib/lua5.3/liblua.so \
		-Bbuild
	make -C build
}

check() {
	make -C build check
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="c5ef1e8dc593e7783b895d22143154aea8f211beeda24672a7ee4ed5112b4f4284043f848a151f3d3c4f569e91308670367a4353f705b20511b36495b22fa3f5  awesome-4.3.tar.xz"
