#!/bin/sh -e
conf=/etc/lilo/lilo.conf

# Check whether LILO is installed
# This function is from /usr/sbin/mkboot from debianutils, with copyright:
#
#   Debian GNU/Linux
#   Copyright 1996-1997 Guy Maor <maor@debian.org>
#
# Modified for Gentoo for use with the lilo ebuild by:
#   Martin Schlemmer <azarah@gentoo.org> (16 Mar 2003)
#
# Modified for Adélie for use with the lilo APKBUILD by:
#   Max Rees <maxcrees@me.com> (19 Mar 2020)
lilocheck() {
	if ! [ -e "$conf" ]; then
		cat >&2 <<-EOF
		*
		* Could not find '$conf'!
		*
		EOF
		exit 1
	fi

	if grep -q "^[[:space:]]*password[[:space:]]*=[[:space:]]*\"\"" \
			"$conf"; then
		cat >&2 <<-EOF
		*
		* You have requested interactive LILO password setup.
		* Run "lilo -p" by hand.
		*
		EOF
		return 1
	fi

	bootpart="$(sed -n "s:^boot[ ]*=[ ]*\(.*\)[ ]*:\1:p" "$conf")"
	if ! [ -b "$bootpart" ]; then
		cat >&2 <<-EOF
		*
		* Could not find '$bootpart'!
		*
		EOF
		exit 1
	fi

	if ! dd if="$bootpart" ibs=16 count=1 2>/dev/null | grep -q LILO; then
		cat >&2 <<-EOF
		*
		* No LILO signature found on '$bootpart'.
		* You must run 'lilo' yourself.
		*
		EOF
		return 1
	fi
}

if ! [ -e /etc/fstab ]; then
	cat >&2 <<-EOF
	*
	* You are missing an /etc/fstab file, so liloconfig
	* cannot determine the root filesystem. Skipping
	* automatic configuration.
	*
	EOF
	exit 0
fi

if [ -e "$conf" ] && [ "$conf" -nt "$conf.template" ]; then
	cat >&2 <<-EOF
	*
	* You appear to have manually edited '$conf'.
	* LILO configuration will not be automatically regenerated.
	*
	EOF
	exit 0
fi

cat >&2 <<-EOF
*
* Running liloconfig...
*
EOF
liloconfig -f "$conf"
touch -r "$conf.template" "$conf"

if lilocheck; then
	cat >&2 <<-EOF
	*
	* Running lilo...
	*
	EOF
	lilo -C "$conf"
fi
