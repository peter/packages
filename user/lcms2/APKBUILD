# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=lcms2
pkgver=2.12
pkgrel=0
pkgdesc="Colour management engine using ICC standard"
url="http://www.littlecms.com/"
arch="all"
license="MIT"
depends=""
depends_dev="libjpeg-turbo-dev tiff-dev zlib-dev"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-utils"
source="https://github.com/mm2/Little-CMS/releases/download/lcms$pkgver/$pkgname-$pkgver.tar.gz"

# secfixes:
#   2.11-r0:
#   - CVE-2018-16435
#   2.8-r1:
#   - CVE-2016-10165

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-static \
		--with-jpeg \
		--with-tiff \
		--with-zlib \
		--with-threads
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="Utility applications for lcms2"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="967e8ac9a1d1aa3be45dc82362b9bc71c555e8577441efda57dc12d0bf84ed9188460c52eb8542d399ce9ab43bd4191988ed22b254ef34c6c1877bbb935952ed  lcms2-2.12.tar.gz"
