# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=alkimia
pkgver=8.0.4
pkgrel=0
pkgdesc="Library for common financial functionality"
url="https://community.kde.org/Alkimia/libalkimia"
arch="all"
options="!check"  # WebKit complains about the DOM
license="LGPL-2.1+"
depends=""
depends_dev="gmp-dev qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtwebkit-dev
	     kconfig-dev kcoreaddons-dev kdelibs4support-dev knewstuff-dev
	     kpackage-dev plasma-framework-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/alkimia/$pkgver/alkimia-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="49b771ac28ec76143cf1237a21a834fa38689c91c773a472cc090e94f49287a79ad7790ef068cd6bebc4e64aa97e5026a9ea5be841bbdb3ca89b00b6ca7acc27  alkimia-8.0.4.tar.xz"
