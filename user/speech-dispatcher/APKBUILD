# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=speech-dispatcher
pkgver=0.11.1
pkgrel=0
pkgdesc="High-level interface to speech synthesis libraries"
url="https://freebsoft.org/speechd"
arch="all"
license="LGPL-2.1-only AND (GPL-2.0+ AND LGPL-2.1+) AND LGPL-2.1+ AND GPL-2.0+"
depends="flite"
# ltdl
makedepends="alsa-lib-dev dotconf-dev flite-dev glib-dev intltool
	libsndfile-dev libtool pulseaudio-dev texinfo"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://github.com/brailcom/speechd/releases/download/$pkgver/speech-dispatcher-$pkgver.tar.gz
	sys-types.patch
	threadleak.patch
	"

build() {
	LIBS="-lasound" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	# gettext-tiny :(
	cp src/api/python/speechd_config/speechd.desktop.in \
		src/api/python/speechd_config/speechd.desktop
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="859911d7db5660423bc7911eacbe8e7a9c3104df97478cbbca48ca5fd6ec018113172d49bc66781680433c77b4d2af43578c9b0f11409e2ba7ac618deb31d7cf  speech-dispatcher-0.11.1.tar.gz
111562b044834189016a0328240f1bc97c38461fb07677b9c6ae36b97b2ddcf3b30c9ac5b8ab6e69209afb8e4e9b6ca13373a685c840be9ba301e1ef270f1a99  sys-types.patch
201e9c1453eb6025e4385173b8068fa8198087d5006f5e90814198b271f7a49aa204235ca7ed521757702e72531678f9cade793d5f4ef1e211122cb9aaf99caa  threadleak.patch"
