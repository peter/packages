# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer:
pkgname=cgit
pkgver=1.2.3
pkgrel=0
_gitver=2.25.1
pkgdesc="A fast Web interface for Git"
url="https://git.zx2c4.com/cgit/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="openssl-dev zlib-dev lua5.3-libs lua5.3-dev asciidoctor gettext-tiny-dev"
subpackages="$pkgname-doc"
source="http://git.zx2c4.com/$pkgname/snapshot/$pkgname-$pkgver.tar.xz
	https://www.kernel.org/pub/software/scm/git/git-$_gitver.tar.xz
	"

# Git makeopts come first
_makeopts="NO_CURL=1
	NO_REGEX=NeedsStartEnd
	NO_SVN_TESTS=YesPlease
	NO_SYS_POLL_H=1
	NO_TCLTK=YesPlease
	ASCIIDOC=asciidoctor
	LUA_PKGCONFIG=lua
	prefix=/usr"

prepare() {
	# check that upstream git ver corresponds with ours
	local _ver="$(awk -F'[ \t]*=[ \t]*' '/^GIT_VER/ { print $2 }' Makefile)"
	if [ "$_ver" != "$_gitver" ]; then
		error "Please set _gitver in APKBUILD to $_ver"
		return 1
	fi

	rm -rf git
	mv ../git-$_gitver git

	default_prepare
}

build() {
	sed -i -e 's:a2x -f:asciidoctor -b:' Makefile
	make $_makeopts all doc-man
}

check() {
	make $_makeopts test
}

package() {
	make $_makeopts DESTDIR="$pkgdir" \
		CGIT_SCRIPT_PATH=/usr/share/webapps/cgit \
		install install-man
	ln -s cgit.cgi "$pkgdir"/usr/share/webapps/cgit/cgit
}

sha512sums="58f9bb644b07be49dc51f3ef30a3d0e53699cede3c06b1d6920f3874fe846c83dd2589632aa84357b70ea2d60272448409aa1b892f405d14dd6745f5559b4504  cgit-1.2.3.tar.xz
15241143acfd8542d85d2709ac3c80dbd6e8d5234438f70c4f33cc71a2bdec3e32938df7f6351e2746d570b021d3bd0b70474ea4beec0c51d1fc45f9c287b344  git-2.25.1.tar.xz"
