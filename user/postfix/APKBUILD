# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=postfix
pkgver=3.7.3
pkgrel=0
pkgdesc="Mail server (MTA) that is Sendmail outside and Qmail inside"
url="http://www.postfix.org/"
arch="all"
pkgusers="postfix"
pkggroups="postfix postdrop"
options="!check suid"  # No test suite.
license="EPL-2.0 AND IPL-1.0"
depends=""
makedepends="cyrus-sasl-dev db-dev icu-dev linux-pam-dev lmdb-dev openldap-dev
	openssl-dev pcre-dev perl postgresql-dev sqlite-dev m4"
subpackages="$pkgname-doc $pkgname-ldap $pkgname-lmdb $pkgname-openrc
	$pkgname-pgsql $pkgname-sqlite"
install="$pkgname.pre-install"
provider_priority=1
provides="sendmail"
source="http://cdn.postfix.johnriley.me/mirrors/postfix-release/official/$pkgname-$pkgver.tar.gz
	honour-config-directory.patch
	linux-6.patch
	postfix.initd
	"

build() {
	local CCARGS="-DHAS_LDAP -DHAS_LMDB -DHAS_PCRE -DHAS_PGSQL -I$(pg_config --includedir) -DHAS_SQLITE -DNO_NIS -DUSE_CYRUS_SASL -DUSE_SASL_AUTH -I/usr/include/sasl -DUSE_LDAP_SASL -DUSE_TLS -Wno-comment"
	local AUXLIBS="-lpam $(pcre-config --libs) -lsasl2 -lssl -lcrypto"
	make DEBUG="" shared=yes dynamicmaps=yes pie=yes \
		config_directory="/etc/postfix" \
		daemon_directory="/usr/libexec/postfix" \
		meta_directory="/etc/postfix" \
		shlib_directory="/usr/lib/postfix" \
		CC="gcc" OPT="$CFLAGS" CCARGS="$CCARGS" AUXLIBS="$AUXLIBS" \
		AUXLIBS_LDAP="-lldap -llber" AUXLIBS_LMDB="-llmdb" \
		AUXLIBS_PCRE="$(pcre-config --libs)" \
		AUXLIBS_PGSQL="-L$(pg_config --libdir) -lpq" \
		AUXLIBS_SQLITE="$(pkgconf --libs sqlite3)" \
		makefiles
}

package() {
	make install_root="$pkgdir" manpage_directory="/usr/share/man" \
		command_directory="/usr/sbin" mailq_path="/usr/bin/mailq" \
		newaliases_path="/usr/bin/newaliases" \
		sendmail_path="/usr/sbin/sendmail" \
		non-interactive-package

	mkdir -p "$pkgdir"/etc/apk/protected_paths.d
	cat > "$pkgdir"/etc/apk/protected_paths.d/postfix.list <<-EOF
		!var/lib/postfix
		!var/spool/postfix
	EOF

	chown -R postfix:postfix \
		"$pkgdir"/var/lib/postfix
	chown -R postfix:postfix \
		"$pkgdir"/var/spool/postfix
	chgrp postdrop "$pkgdir"/var/spool/postfix/public \
		"$pkgdir"/var/spool/postfix/maildrop
	chmod 0750 "$pkgdir"/var/lib/postfix
	chgrp postdrop "$pkgdir"/usr/sbin/postdrop "$pkgdir"/usr/sbin/postqueue
	chmod 2755 "$pkgdir"/usr/sbin/postdrop "$pkgdir"/usr/sbin/postqueue

	install -D -m755 "$srcdir"/postfix.initd "$pkgdir"/etc/init.d/postfix
}

_database() {
	pkgdesc="Postfix mapping plugin for storing users in $2"

	install -d "$subpkgdir"/etc/postfix/dynamicmaps.cf.d
	grep $1 "$pkgdir"/etc/postfix/dynamicmaps.cf > \
		"$subpkgdir"/etc/postfix/dynamicmaps.cf.d/$1
	sed -i "/$1/d" "$pkgdir"/etc/postfix/dynamicmaps.cf
	install -d "$subpkgdir"/usr/lib/postfix
	mv "$pkgdir"/usr/lib/postfix/postfix-$1.so "$subpkgdir"/usr/lib/postfix/
}

ldap() {
	_database "ldap" "LDAP"
}

lmdb() {
	_database "lmdb" "LMDB"
}

pgsql() {
	_database "pgsql" "PostgreSQL"
}

sqlite() {
	_database "sqlite" "SQLite 3"
}

sha512sums="4ceedd1b7b364f47a3becc041cf29a48aea54e38306fd1227c5a7c25894831fb5c37150d99d781d237175e58da21ac53887c97d99bb6b715c4988777596ee890  postfix-3.7.3.tar.gz
63ea7fe461fca1306088e9984acfab6ab3e919dafa2b606d43fbca472969e6ec232800071f939f5a4482c01c6d7b53a86de9e7e893ab94f4cb135d16ff5763cc  honour-config-directory.patch
aa9e47208041d652351ce167fe6205116658704189bc2a715a3edb31c0436d46fad909cfe57e1d6d869c933ff0b5237be0956b38a94b395781998c9092305274  linux-6.patch
2ad1b053bbb51d4f480dbf5a01252e8e99a82ec14a75ff49336b2b190ea55bc02400542b109fa0cc4ebfe6b42eaabbc45f70f2ea581f1eb9c4189c439b825592  postfix.initd"
