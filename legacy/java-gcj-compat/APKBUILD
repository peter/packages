# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
gccver=6.4.0
gccpkgrel=10

pkgname=java-gcj-compat
pkgver=$gccver
pkgrel=$gccpkgrel
pkgdesc="Java runtime environment using GCC Java"
url="https://gcc.gnu.org/"
arch="all"
license="GPL-3.0+ AND LGPL-2.1+"
depends="fastjar gcc-java=$gccver-r$gccpkgrel java-common"
makedepends="gcc-java=$gccver-r$gccpkgrel zlib-dev"
subpackages=""
source=""

build() {
	mkdir -p $builddir
	cd $builddir
	msg "Building ecj..."
	gcj -Wl,-Bsymbolic -findirect-dispatch -o ecj \
		--main=org.eclipse.jdt.internal.compiler.batch.Main \
		/usr/share/java/ecj.jar -lgcj
}

check() {
	$builddir/ecj > /dev/null
}

package() {
	JVM_DIR=$pkgdir/usr/lib/jvm
	JDK_DIR=$JVM_DIR/java-1.5-gcj
	JDK_BIN_DIR=$JDK_DIR/bin
	JRE_DIR=$JDK_DIR/jre
	JRE_BIN_DIR=$JRE_DIR/bin
	JRE_LIB_DIR=$JRE_DIR/lib

	CPU=$(uname -m | sed -e 's/i.86/i386/g' -e 's/x86_64/amd64/g')

	mkdir -p $JDK_BIN_DIR
	ln -sf ../../../../bin/gij $JDK_BIN_DIR/java
	ln -sf ../../../../bin/fastjar $JDK_BIN_DIR/fastjar
	ln -sf ../../../../bin/sinjdoc $JDK_BIN_DIR/javadoc
	ln -sf ../../../../bin/grmic $JDK_BIN_DIR/rmic
	ln -sf ../../../../bin/gjavah $JDK_BIN_DIR/javah
	install -s $builddir/ecj $JDK_BIN_DIR/javac
	ln -sf ../../../../bin/gappletviewer $JDK_BIN_DIR/appletviewer
	ln -sf ../../../../bin/gjarsigner $JDK_BIN_DIR/jarsigner
	ln -sf ../../../../bin/grmiregistry $JDK_BIN_DIR/rmiregistry
	ln -sf ../../../../bin/gkeytool $JDK_BIN_DIR/keytool

	gcjrel=`ls -d /usr/lib/gcj-$gccver-* | cut -d '-' -f 3`

	mkdir -p $JRE_BIN_DIR $JRE_LIB_DIR/$CPU
	ln -sf ../../../../../bin/gij $JRE_BIN_DIR/java
	ln -sf ../../../../../bin/grmiregistry $JRE_BIN_DIR/rmiregistry
	ln -sf ../../../../../bin/gkeytool $JRE_BIN_DIR/keytool
	ln -sf ../../../../../share/java/libgcj-$gccver.jar $JRE_LIB_DIR/rt.jar
	ln -sf ../../../../../share/java/libgcj-tools-$gccver.jar $JRE_LIB_DIR/tools.jar
	ln -sf ../../../../../../lib/gcj-$gccver-$gcjrel/libjvm.so $JRE_LIB_DIR/$CPU
	ln -sf ../../../../../../lib/gcj-$gccver-$gcjrel/libjavamath.so $JRE_LIB_DIR/$CPU
	ln -sf ../../../../../../lib/gcj-$gccver-$gcjrel/classmap.db $JRE_LIB_DIR/$CPU
}

